package differentiator;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
//import org.antlr.v4.runtime.tree.ParseTreeListener;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import differentiator.grammar.ExpressionLexer;
import differentiator.grammar.ExpressionParser;

/** Symbolic differentiator */
public class Differentiator implements DifferentiatorInterface {
    /**
     * See the spec on DifferentiatorInterface
     */
    public String apply(String expression, String variable) {
        // Recursive Differentiation (return the tree of the resulting AST)
        Expression root = parse(expression);
        Variable var = new Variable(variable);
        Expression differential = root.differentiate(var);
        return differential.toString();
    }
    
    /**
     * See the spec on DifferentiatorInterface
     */
    public Expression parse(String expression) {
        // Create an AST from a String expression
        return runListener(expression);
    }
    
    
    /**
     * Runs the listener on the given input string.
     * 
     * You should not make major modifications to this method.
     * 
     * @param input The input string.
     */
    private Expression runListener(String input) {
        // Create a stream of tokens using the lexer.
        CharStream stream = new ANTLRInputStream(input);
        ExpressionLexer lexer = new ExpressionLexer(stream);
        lexer.reportErrorsAsExceptions();
        TokenStream tokens = new CommonTokenStream(lexer);
        
        // Feed the tokens into the parser.
        ExpressionParser parser = new ExpressionParser(tokens);
        parser.reportErrorsAsExceptions();
        
        // Generate the parse tree using the starter rule.
        ParseTree tree;
        tree = parser.line(); // "line" is the starter rule.
        
        // Walk the tree with the listener.
        ParseTreeWalker walker = new ParseTreeWalker();
        
        // CHANGED: used my own Listener
        Listener listener = new Listener();
        walker.walk(listener, tree);
        
        // CHANGED: now outputs and expression
        return listener.getExpression();
        
    }


}
