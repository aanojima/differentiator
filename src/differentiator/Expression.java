package differentiator;

/**
 * Expression is an immutable datatype representing 
 * a differentiable expression.  Expressions consist of
 * bases such as constant integers/floats and variables.  
 * Also consist of constructors such as additions and 
 * multiplications.  Bases consists of immutable fields
 * such as primitive data types and strings.  Each base 
 * takes one parameter and each constructor takes in 
 * exactly two parameters.  Constructors take base 
 * expressions or complex expressions to form new 
 * complex expressions.  Each expression has a value, type, 
 * a left and/or right child expression, a string representation, 
 * an equality test, and a hashCode representation.  
 */

/*
 * An interface requiring the toString(), equals(), and hashCode() methods.
 *
 * <b>DO NOT MODIFY THIS INTERFACE!</b>
 */

public interface Expression {
    // DO NOT change the name of this interface or move it to a different package.
    // You may, however, add methods to this interface.
    
    
    /**
     * Simplifies the expression as a double.  Will combine terms.  
     * A variable cannot be simplified and the expression will return
     * NaN for these case scenarios.  
     * @return the value of the expression as either a double or NaN
     */
    public double getValue();

    
    /**
     * Specifies the type of the Expression (the classs)
     * @return string name of the type of expression
     */
    public String getType();
    
    
    /**
     * Gets the right expression for addition and multiplication.  
     * For constants and variables, will return the constant or variable
     * and acts the same as getLeft()
     * @return the right expression
     */
    public Expression getRight();    
    
    
    /**
     * Gets the left expression for addition and multiplication.  
     * For constants and variables, will return the constant or variable
     * and acts the same as getRight()
     * @return the left expression
     */
    public Expression getLeft();
    
    
    /**
     * Converts the Expression into a readable String format
     * including constants (integer and float), variables,
     * mathematical operations like '+' or '*', and
     * parentheses grouping.  
     * 
     * @return a string that represents the expression
     */
    public String toString();
    
    
    /**
     * Evaluates if the the expression equals another expression
     * based on structural equality.  Three axioms must hold true:
     * 
     * Reflective: A = A
     * Symmetric: A = B implies that B = A
     * Transitive: A = B and B = C implies that A = C
     * 
     * @param expression - this is the expression to compare to.  
     * @return boolean stating whether the expression matches other
     * specified expression
     */
    public boolean equals(Expression expression);
    
    
    /**
     * Converts the expression into an integer unique to the mathematical
     * expression equivalent.  Order doesn't matter.  Any algebraic equivalent
     * in terms of integers or doubles (doesn't matter which) will produce the
     * same hash code.  
     * 
     * @return integer representation unique to the algebraic value of the expression.  
     */
    public int hashCode();
    
    
    /**
     * Differentiates the expression with respect to a specified variable.  
     * The variable must be of the Expression Variable class.  The expression
     * to be differentiated may contain no or many instances of the variable
     * as well as other different variables, constants, and operations.  The 
     * result or output will be a differentiated expression.  
     * 
     * @param variable - Variable the expression will be differentiated with 
     * respect to.  Variable can be named anything desired.    
     * @return an expression that represents the differentiation of the 
     * initial expression.  
     */
    public Expression differentiate(Expression variable);

}
