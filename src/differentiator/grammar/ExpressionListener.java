// Generated from Expression.g4 by ANTLR 4.0

package differentiator.grammar;

import org.antlr.v4.runtime.tree.*;
import org.antlr.v4.runtime.Token;

public interface ExpressionListener extends ParseTreeListener {
	void enterExpression(ExpressionParser.ExpressionContext ctx);
	void exitExpression(ExpressionParser.ExpressionContext ctx);

	void enterAtom(ExpressionParser.AtomContext ctx);
	void exitAtom(ExpressionParser.AtomContext ctx);

	void enterMultiplication(ExpressionParser.MultiplicationContext ctx);
	void exitMultiplication(ExpressionParser.MultiplicationContext ctx);

	void enterLine(ExpressionParser.LineContext ctx);
	void exitLine(ExpressionParser.LineContext ctx);
}