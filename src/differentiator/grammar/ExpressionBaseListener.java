// Generated from Expression.g4 by ANTLR 4.0

package differentiator.grammar;


import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.antlr.v4.runtime.tree.ErrorNode;

public class ExpressionBaseListener implements ExpressionListener {
	@Override public void enterExpression(ExpressionParser.ExpressionContext ctx) { }
	@Override public void exitExpression(ExpressionParser.ExpressionContext ctx) { }

	@Override public void enterAtom(ExpressionParser.AtomContext ctx) { }
	@Override public void exitAtom(ExpressionParser.AtomContext ctx) { }

	@Override public void enterMultiplication(ExpressionParser.MultiplicationContext ctx) { }
	@Override public void exitMultiplication(ExpressionParser.MultiplicationContext ctx) { }

	@Override public void enterLine(ExpressionParser.LineContext ctx) { }
	@Override public void exitLine(ExpressionParser.LineContext ctx) { }

	@Override public void enterEveryRule(ParserRuleContext ctx) { }
	@Override public void exitEveryRule(ParserRuleContext ctx) { }
	@Override public void visitTerminal(TerminalNode node) { }
	@Override public void visitErrorNode(ErrorNode node) { }
}