package differentiator;

public class Multiplication implements Expression {
    
    private final Expression left;
    private final Expression right;
    private final String type;
    
    @Override
    public Expression getLeft(){
        return this.left;
    }
    
    @Override
    public Expression getRight(){
        return this.right;
    }
    
    @Override
    public String getType(){
        return this.type;
    }
    
    @Override
    public double getValue(){
        // Continues to Return NaN values if branches contained variables. Otherwise 
        // return the product of previous expressions
        if (this.left.getValue() == 0.0 || this.right.getValue() == 0.0){
            return 0.0;
        } else if (this.left.getValue() == Double.NaN || this.right.getValue() == Double.NaN){
            return Double.NaN;
        } else {
            return this.left.getValue() * this.right.getValue();
        }
    }
    
    public Multiplication(Expression left, Expression right){
        this.left = left;
        this.right = right;
        this.type = "Multiplication";
    }
    
    @Override
    public String toString(){
        if (this.getValue() == 0){
            return "";
        } else if (this.left.getValue() != 1.0 && this.right.getValue() != 1.0){
            return "(" + left.toString() + " * " + right.toString() + ")";
        } else if (this.left.getValue() == 1.0){
            return this.right.toString();
        } else {
            return this.left.toString();
        }
    }
    
    @Override
    public boolean equals(Expression expression){
        return this.getClass() == expression.getClass() && 
                this.left.equals(expression.getLeft()) && 
                this.right.equals(expression.getRight());
    }
    
    @Override
    public int hashCode(){
        if (this.getValue() != Double.NaN){
            return new Double(this.getValue()).hashCode();
        } else {
            return this.left.hashCode() * this.right.hashCode();
        }
    }
    
    @Override
    public Expression differentiate(Expression variable){
        Expression output = new 
                Addition(
                    new Multiplication(
                        this.left,
                        this.right.differentiate(variable)
                    ),
                    new Multiplication(
                        this.right,
                        this.left.differentiate(variable)
                    )
                );
        return output;
    }
}
