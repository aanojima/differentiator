package differentiator;

public class Constant implements Expression {
    
    private final int intValue;
    private final double doubleValue;
    private final String type;
        
    @Override
    public Expression getLeft(){
        return this;
    }
    
    @Override
    public Expression getRight(){
        return this;
    }
    
    @Override
    public String getType(){
        return this.type;
    }

    @Override
    public double getValue(){
        if (this.type.equals("int")){
            return (double)this.intValue;
        } else {
            return this.doubleValue;
        }
    }
    
    // Two Constructor Methods: one for integers, one for doubles
    
    public Constant(int intValue){
        this.doubleValue = (double)intValue;
        this.intValue = intValue;
        this.type = "int";
    }

    public Constant(double doubleValue){
        this.intValue = (int)doubleValue;
        this.doubleValue = doubleValue;
        this.type = "double";
    }
    
    @Override
    public String toString(){
        if (this.type.equals("int")){
            return Integer.toString(this.intValue);
        } else {
            return Double.toString(this.doubleValue);
        }
    }
    
    @Override
    public boolean equals(Expression expression){
        return this.getType().equals(expression.getType()) && this.hashCode() == expression.hashCode();
    }
    
    @Override
    public int hashCode(){
        if (this.getType().equals("int")){
            return new Double(this.intValue).hashCode();
        } else {
            return new Double(this.doubleValue).hashCode();
        }
    }
    
    @Override
    public Expression differentiate(Expression variable){
        return new Constant(0);
    }
}
