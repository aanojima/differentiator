package differentiator;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.junit.Test;

import differentiator.grammar.ExpressionLexer;

/**
 * This is the test suite for Lexer. You should expand this file with more
 * unit tests to make sure your Lexer works correctly.
 */
public class LexerTest {

    @Test
    public void testPlusToken() {
        verifyLexer("+*", new String[] {"+", "*"});
    }
    
    @Test
    public void testTimesToken() {
        verifyLexer("*", new String[] {"*"});
    }
    
    @Test
    public void testParenthesesToken() {
        verifyLexer("(", new String[] {"("});
        verifyLexer(")", new String[] {")"});
    }
    
    @Test
    public void testDoubleParenthesesToken() {
        verifyLexer("()", new String[] {"(", ")"});
    }
    
//    @Test
    public void testNumberTokens() {
        verifyLexer("0", new String[] {"0"});
        verifyLexer("12345", new String[] {"12345"});
        verifyLexer(" 9876 ", new String[] {"9876"});
        verifyLexer("2.4", new String[] {"2.4"});
        verifyLexer(".98", new String[] {".98"});
        verifyLexer("01234566789.9876543210", new String[] {"01234566789.9876543210"});
    }
    
    @Test
    public void testExpressionTokens() {
        verifyLexer("(2*x)+(y*x)", new String[] {"(","2","*","x",")","+","(","y","*","x",")"});
    }
    
    public void verifyLexer(String input, String[] expectedTokens) {
        CharStream stream = new ANTLRInputStream(input);
        ExpressionLexer lexer = new ExpressionLexer(stream);
        lexer.reportErrorsAsExceptions();
        List<? extends Token> actualTokens = lexer.getAllTokens();
        
        assertEquals(expectedTokens.length, actualTokens.size());
        
        for(int i = 0; i < actualTokens.size(); i++) {
             String actualToken = actualTokens.get(i).getText();
             String expectedToken = expectedTokens[i];
             System.out.println(actualToken);
             assertEquals(actualToken, expectedToken);
             
        }
    }

    
}