package differentiator;

import static org.junit.Assert.*;

import org.junit.Test;

public class ExpressionTest {

    @Test
    public void testConstantToString() {
        
        // Zero Integer
        Expression zeroInt = new Constant(0);
        assertEquals("0", zeroInt.toString());
        
        // Zero Float
        Expression zeroFloat = new Constant(0.0);
        assertEquals("0.0", zeroFloat.toString());
                
        // One Integer
        Expression intConstant = new Constant(1);
        assertEquals("1", intConstant.toString());
        
        // One Float
        Expression floatConstant = new Constant(1.0);
        assertEquals("1.0", floatConstant.toString());
        
        // Negative One Integer
        Expression negativeInteger = new Constant(-1);
        assertEquals("-1", negativeInteger.toString());
        
        // Negative One Float
        Expression negativeFloat = new Constant(-1.0);
        assertEquals("-1.0", negativeFloat.toString());
        
        // Max Integer
        Expression maxInt = new Constant(Integer.MAX_VALUE);
        assertEquals("2147483647", maxInt.toString());
        
        // Min Integer
        Expression minInt = new Constant(Integer.MIN_VALUE);
        assertEquals("-2147483648", minInt.toString());
        
        // Positive Infinity
        Expression posInfinity = new Constant(1./0);
        assertEquals("Infinity", posInfinity.toString());
        
        // Negative Infinity
        Expression negInfinity = new Constant(-1./0);
        assertEquals("-Infinity", negInfinity.toString());
        
    }
    
    @Test
    public void testVariableToString() {
        
        // Creation of Multiple Variables
        Expression variableX = new Variable("x");
        Expression variableY = new Variable("y");
        assertEquals("x", variableX.toString());
        assertEquals("y", variableY.toString());        
        
    }
    
    @Test
    public void testAdditionExpressionToString() {
        
        Expression variableX = new Variable("x");
        Expression variableY = new Variable("y");
        Expression constantInt = new Constant(1);
        Expression constantFlt = new Constant(1.0);
        
        // Addition of Variable and Constant Integer
        Expression addition = new Addition(variableX, constantInt);
        assertEquals("(x + 1)", addition.toString());
        
        // Addition of Variable and Constant Float
        Expression addition2 = new Addition(variableY, constantFlt);
        assertEquals("(y + 1.0)", addition2.toString());
        
        // Addition of Two Variables
        Expression addition3 = new Addition(variableX, variableY);
        assertEquals("(x + y)", addition3.toString());
        
        // Addition of Two Constant Integers
        Expression addition4 = new Addition(constantInt, constantInt);
        assertEquals("(1 + 1)", addition4.toString());
        
        // Addition of Two Constant Floats
        Expression addition5 = new Addition(constantFlt, constantFlt);
        assertEquals("(1.0 + 1.0)", addition5.toString());
        
        // Addition of Constant Integer and Constant Float
        Expression addition6 = new Addition(constantInt, constantFlt);
        assertEquals("(1 + 1.0)", addition6.toString());
        
        // Addition of Two Addition Expressions
        Expression addition7 = new Addition(addition, addition2);
        assertEquals("((x + 1) + (y + 1.0))", addition7.toString());
    }
    
    @Test
    public void testMultiplicationExpressionToString() {
        
        Expression variableX = new Variable("x");
        Expression variableY = new Variable("y");
        Expression constantInt = new Constant(2);
        Expression constantFlt = new Constant(2.0);
        
        // Multiply Constant Integer and Variable
        Expression multiplication = new Multiplication(constantInt, variableX);
        assertEquals("(2 * x)", multiplication.toString());
        
        // Multiply Constant Float and Variable
        Expression multiplication2 = new Multiplication(constantFlt, variableY);
        assertEquals("(2.0 * y)", multiplication2.toString());
        
        // Multiply Two Same Variables
        Expression multiplication3 = new Multiplication(variableX, variableX);
        assertEquals("(x * x)", multiplication3.toString());
        
        // Multiply Two Different Variables
        Expression multiplication4 = new Multiplication(variableX, variableY);
        assertEquals("(x * y)", multiplication4.toString());
        
        // Multiply Two Constant Integers
        Expression multiplication5 = new Multiplication(constantInt, constantInt);
        assertEquals("(2 * 2)", multiplication5.toString());
        
        // Multiply Two Constant Floats
        Expression multiplication6 = new Multiplication(constantFlt, constantFlt);
        assertEquals("(2.0 * 2.0)", multiplication6.toString());
        
        // Multiply Constant Integer and Constant Float
        Expression multiplication7 = new Multiplication(constantInt, constantFlt);
        assertEquals("(2 * 2.0)", multiplication7.toString());
        
    }
    
    @Test
    public void testMixedOperationsExpressionToString() {
        
        Expression variable = new Variable("x");
        Expression constant = new Constant(2);
        Expression multiplication = new Multiplication(constant, variable);
        Expression addition = new Addition(variable, constant);
        
        Expression compoundAddition = new Addition(multiplication, constant);
        Expression compoundMultiplication = new Multiplication(addition, constant);
        
        assertEquals("((2 * x) + 2)", compoundAddition.toString());
        assertEquals("((x + 2) * 2)", compoundMultiplication.toString());
        
    }

    @Test
    public void testConstantEquals() {
        
        Expression constant1 = new Constant(1);
        Expression constant1a = new Constant(1);
        Expression constant2 = new Constant(2);
        Expression constant1d0001 = new Constant(1.0001);
        Expression constant1d0002 = new Constant(1.0002);
        
        assertEquals(true, constant1.equals(constant1));
        assertEquals(false, constant1.equals(constant2));
        assertEquals(false, constant1d0001.equals(constant1d0002));
        assertEquals(true, constant1.equals(constant1a));
        
    }
    
    @Test
    public void testConstantAdditionEquals() {
        
        Expression constant1d0001 = new Constant(1.0001);
        Expression constant1d0002 = new Constant(1.0002);
        
        // Addition of Constant and Constant
        Expression expression1 = new Addition(new Constant(1), new Constant(2));
        Expression reversed1 = new Addition(new Constant(2), new Constant(1));
        assertEquals(false, expression1.equals(reversed1));
        
        Expression copy1 = new Addition(new Constant(1), new Constant(2));
        assertEquals(true, expression1.equals(copy1));
        
        expression1 = new Addition(constant1d0001, constant1d0002);
        reversed1 = new Addition(constant1d0002, constant1d0002);
        assertEquals(false, expression1.equals(reversed1));
        
        Expression expression1a = new Addition(new Constant(1.0), new Constant(1));
        Expression expression1b = new Addition(new Constant(1), new Constant(1.0));
        assertEquals(false,expression1a.equals(expression1b));
    }
    
    @Test
    public void testConstantVariableAdditionEquals() {
        
        Expression constant1d0001 = new Constant(1.0001);
        
        // Addition of Constant and Variable
        Expression expression2 = new Addition(new Constant(1), new Variable("x"));
        Expression reversed2 = new Addition(new Variable("x"), new Constant(1));
        assertEquals(false, expression2.equals(reversed2));
        
        assertEquals(true, expression2.equals(expression2));
        Expression copy2 = new Addition(new Constant(1), new Variable("x"));
        assertEquals(true, expression2.equals(copy2));
        
        expression2 = new Addition(constant1d0001, new Variable("x"));
        reversed2 = new Addition(new Variable("x"), constant1d0001);
        assertEquals(false, expression2.equals(reversed2));

    }
    
    @Test
    public void testSameVariableAdditionEquals() {
        
        // Addition of Two Same Variables
        Expression expression3 = new Addition(new Variable("x"), new Variable("x"));
        Expression reversed3 = new Addition(new Variable("x"), new Variable("x"));
        assertEquals(true, expression3.equals(reversed3));
        
    }
    
    @Test
    public void testDiffVariableAdditionEquals() {
        
        // Addition of Two Different Variables
        Expression expression4 = new Addition(new Variable("y"), new Variable("x"));
        Expression reversed4 = new Addition(new Variable("x"), new Variable("y"));
        assertEquals(false, expression4.equals(reversed4));
        
        Expression copy4 = new Addition(new Variable("y"), new Variable("x"));
        assertEquals(true, expression4.equals(copy4));
       
    }
    
    @Test
    public void testConstantMultiplicationEquals() {
        
        Expression constant1d0001 = new Constant(1.0001);
        Expression constant1d0002 = new Constant(1.0002);
        
        // Multiplication of Constant and Constant
        Expression expression5 = new Multiplication(new Constant(1), new Constant(2));
        Expression reversed5 = new Multiplication(new Constant(2), new Constant(1));
        assertEquals(false, expression5.equals(reversed5));
        
        Expression copy5 = new Multiplication(new Constant(1), new Constant(2));
        assertEquals(true, expression5.equals(copy5));
        
        expression5 = new Multiplication(constant1d0001, constant1d0002);
        reversed5 = new Multiplication(constant1d0002, constant1d0001);
        assertEquals(false, expression5.equals(reversed5));
        
        Expression expression5a = new Multiplication(new Constant(1.0), new Constant(1));
        Expression expression5b = new Multiplication(new Constant(1), new Constant(1.0));
        assertEquals(false,expression5a.equals(expression5b));
      
    }
    
    @Test
    public void testConstantVariableMultiplicationEquals() {
        
        Expression constant1d0001 = new Constant(1.0001);
        Expression constant1d0002 = new Constant(1.0002);
        
        // Multiplication of Constant and Variable
        Expression expression6 = new Multiplication(new Constant(1), new Variable("x"));
        Expression reversed6 = new Multiplication(new Variable("x"), new Constant(1));
        assertEquals(false, expression6.equals(reversed6));
        
        Expression copy6 = new Multiplication(new Constant(1), new Variable("x"));
        assertEquals(true, expression6.equals(copy6));
        
        expression6 = new Multiplication(constant1d0001, constant1d0002);
        reversed6 = new Multiplication(constant1d0002, constant1d0001);
        assertEquals(false, expression6.equals(reversed6));
        
    }
    
    @Test
    public void testSameVariableMultiplicationEquals() {
        
        // Multiplication of Two Same Variables
        Expression expression7 = new Multiplication(new Variable("x"), new Variable("x"));
        Expression reversed7 = new Multiplication(new Variable("x"), new Variable("x"));
        assertEquals(true, expression7.equals(reversed7));
        
    }

    @Test
    public void testDiffVariablesMultiplicationEquals() {
                
        // Multiplication of Two Different Variables
        Expression expression8 = new Multiplication(new Variable("y"), new Variable("x"));
        Expression reversed8 = new Multiplication(new Variable("x"), new Variable("y"));
        assertEquals(false, expression8.equals(reversed8));
        
        Expression copy8 = new Multiplication(new Variable("y"), new Variable("x"));
        assertEquals(true, expression8.equals(copy8));
        
    }

    @Test
    public void testHashCodeConstants() {

        
        Expression constant1 = new Constant(1);
        Expression constant2 = new Constant(2);
        Expression constant3 = new Constant(3);
        Expression constant1d0 = new Constant(1.0);
        Expression constant1d0000 = new Constant(1.0000);
        Expression constant1d0001 = new Constant(1.0001);
        
        // Reflective Equality
        assertEquals(true, constant1.hashCode() == constant1.hashCode());
        assertEquals(true, constant1.hashCode() == constant1d0000.hashCode());
        assertEquals(false, constant1.hashCode() == constant2.hashCode());
        assertEquals(false, constant1d0000.hashCode() == constant1d0001.hashCode());
        
        // Symmetric Equality
        Expression constant3COPY = new Constant(3);
        assertEquals(true, constant3.hashCode() == constant3COPY.hashCode());
        
        // Transitive Equality
        assertEquals(true, constant1.hashCode() == constant1d0.hashCode());
        assertEquals(true, constant1d0.hashCode() == constant1d0000.hashCode());
        assertEquals(true, constant1d0000.hashCode() == constant1.hashCode());
    }
    
    @Test
    public void testHashCodeVariables() {
        
        // Reflective
        assertEquals(true, new Variable("x").hashCode() == new Variable("x").hashCode());
        assertEquals(false, new Variable("x").hashCode() == new Variable("X").hashCode());
        assertEquals(false, new Variable("x").hashCode() == new Variable("y").hashCode());
        
        // Symmetric
        Expression x = new Variable("x");
        assertEquals(true, x.hashCode() == new Variable("x").hashCode());
        assertEquals(true, new Variable("x").hashCode() == x.hashCode());
        assertEquals(false, x.hashCode() == new Variable("y").hashCode());
        assertEquals(false, new Variable("y").hashCode() == x.hashCode());
        
        // Transitive
        Expression x1 = new Variable("x");
        Expression x2 = new Variable("x");
        Expression x3 = new Variable("x");
        assertEquals(true, x1.hashCode() == x2.hashCode());
        assertEquals(true, x2.hashCode() == x3.hashCode());
        assertEquals(true, x3.hashCode() == x1.hashCode());
    }
    
    @Test
    public void testHashCodeAddition() {
        
        Expression zero = new Constant(0);
        Expression one = new Constant(1);
        Expression two = new Constant(2);
        Expression three = new Constant(3);
        Expression twoF = new Constant(2.5);
        Expression x = new Variable("x");
        Expression y = new Variable("y");
        
        // Reflective
        Expression addition1a = new Addition(one,twoF);
        Expression addition1b = new Addition(one,twoF);
        assertEquals(true, addition1a.hashCode() == addition1b.hashCode());
        
        Expression addition1c = new Addition(three,x);
        Expression addition1d = new Addition(three,x);
        assertEquals(true, addition1c.hashCode() == addition1d.hashCode());
        
        // Symmetric
        Expression addition2a = new Addition(one, twoF);
        Expression addition2b = new Addition(twoF, one);
        assertEquals(true, addition2a.hashCode() == addition2b.hashCode());
        
        Expression addition2c = new Addition(one, y);
        Expression addition2d = new Addition(y, one);
        assertEquals(true, addition2c.hashCode() == addition2d.hashCode());
        
        Expression addition2e = new Addition(twoF, x);
        Expression addition2f = new Addition(x, twoF);
        assertEquals(true, addition2e.hashCode() == addition2f.hashCode());
        
        // Transitive
        Expression addition3a = new Addition(zero,two);
        Expression addition3b = new Addition(one,one);
        Expression addition3c = new Addition(two,zero);
        assertEquals(true, addition3a.hashCode() == addition3b.hashCode());
        assertEquals(true, addition3b.hashCode() == addition3c.hashCode());
        assertEquals(true, addition3c.hashCode() == addition3a.hashCode());

    }
    
    @Test
    public void testHashCodeMultiplication() {
        
        Expression one = new Constant(1);
        Expression two = new Constant(2);
        Expression three = new Constant(3);
        Expression four = new Constant(4);
        Expression six = new Constant(6);
        Expression twelve = new Constant(12);
        Expression oneF = new Constant(1.0);
        Expression twoF = new Constant(2.0);
        Expression threeF = new Constant(3.0);
        Expression x = new Variable("x");
        Expression y = new Variable("y");
        
        // Reflective
        Expression multiplication1a = new Multiplication(one,twoF);
        Expression multiplication1b = new Multiplication(one,twoF);
        assertEquals(true, multiplication1a.hashCode() == multiplication1b.hashCode());

        Expression multiplication1c = new Multiplication(three,x);
        Expression multiplication1d = new Multiplication(three,x);
        assertEquals(true, multiplication1c.hashCode() == multiplication1d.hashCode());

        // Symmetric
        Expression multiplication2a = new Multiplication(two, threeF);
        Expression multiplication2b = new Multiplication(threeF, two);
        assertEquals(true, multiplication2a.hashCode() == multiplication2b.hashCode());

        Expression multiplication2c = new Multiplication(one, y);
        Expression multiplication2d = new Multiplication(y, one);
        assertEquals(true, multiplication2c.hashCode() == multiplication2d.hashCode());

        Expression multiplication2e = new Multiplication(oneF, y);
        Expression multiplication2f = new Multiplication(y, oneF);
        assertEquals(true, multiplication2e.hashCode() == multiplication2f.hashCode());

        // Transitive
        Expression multiplication3a = new Multiplication(one,twelve);
        Expression multiplication3b = new Multiplication(two,six);
        Expression multiplication3c = new Multiplication(three,four);
        assertEquals(true, multiplication3a.hashCode() == multiplication3b.hashCode());
        assertEquals(true, multiplication3b.hashCode() == multiplication3c.hashCode());
        assertEquals(true, multiplication3c.hashCode() == multiplication3a.hashCode());
    }
        
    @Test
    public void testHashCodeMixedOperations() {
        
        Expression one = new Constant(1);
        Expression two = new Constant(2);
        Expression three = new Constant(3);
        Expression four = new Constant(4);
        Expression oneF = new Constant(1.0);
        Expression twoF = new Constant(2.0);
        Expression twoPointFive = new Constant(2.5);
        Expression threeF = new Constant(3.0);
        Expression fourF = new Constant(4.0);
        Expression x = new Variable("x");
        Expression y = new Variable("y");
        
        // Identical Constant Addition and Multiplication
        Expression am1 = new Addition(one,three);
        Expression am2 = new Multiplication(one, four);
        assertEquals(true, am1.hashCode() == am2.hashCode());
        
        // Identical Variable Addition and Multiplication
        Expression xPLUSx = new Addition(x, x);
        Expression TWOx = new Multiplication(two, x);
        assertEquals(true, xPLUSx.hashCode() == TWOx.hashCode());
        
        // Nested Addition
        Expression a1 = new Addition(one, twoF);
        Expression a2 = new Addition(oneF, a1);
        Expression a3 = new Addition(a2, y);
        Expression a4 = new Addition(y, fourF);
        assertEquals(true, a4.hashCode() == a3.hashCode());
        
        // Nested Multiplication
        Expression m1 = new Multiplication(twoPointFive, threeF);
        Expression m2 = new Multiplication(two, m1);
        Expression m3 = new Multiplication(x, m2);
        Expression m4 = new Multiplication(new Constant(15),x);
        assertEquals(true, m4.hashCode() == m3.hashCode());
        
    }
        
    @Test
    public void testDifferentiateZero(){
        
        Expression zero = new Constant(0);
        Expression zeroF = new Constant(0.0);
        Expression x = new Variable("x");
        assertEquals(true, new Constant(0).equals(zero.differentiate(x)));
        assertEquals(true, new Constant(0).equals(zeroF.differentiate(x)));
        
    }
    
    @Test
    public void testDifferentiateConstant(){
        
        Expression c1 = new Constant(1);
        Expression c2 = new Constant(1234567890);
        Expression x = new Variable("x");
        assertEquals(true, new Constant(0).equals(c1.differentiate(x)));
        assertEquals(true, new Constant(0).equals(c2.differentiate(x)));
        
    }
    
    @Test
    public void testDifferentiateVariable(){
        
        Expression x = new Variable("x");
        Expression a = new Variable("a");
        assertEquals(true, new Constant(0).equals(a.differentiate(x)));
        assertEquals(true, new Constant(1).equals(x.differentiate(x)));
        
    }
    
    @Test
    public void testDifferentiateAddition(){
        
        Expression x = new Variable("x");
        
        Expression addition = new Addition(new Variable("x"), new Constant(1));
        Expression expected = new Addition(new Constant(1), new Constant(0));
        assertEquals(true, expected.equals(addition.differentiate(x)));
        
        Expression addition2 = new Addition(new Variable("x"), new Variable("y"));
        Expression expected2 = new Addition(new Constant(1), new Constant(0));
        assertEquals(true, expected2.equals(addition2.differentiate(x)));
        
        Expression addition3 = new Addition(new Variable("x"), new Variable("x"));
        Expression expected3 = new Addition(new Constant(1), new Constant(1));
        assertEquals(true, expected3.equals(addition3.differentiate(x)));
        
    }
    
    @Test
    public void testDiferrentiateMultiplication(){
        
        Expression x = new Variable("x");
        
        // Constant and Variable
        Expression multiplication = new Multiplication(new Constant(2), new Variable("x"));
        Expression expected = new Addition(
                new Multiplication(new Constant(2), new Constant(1)),
                new Multiplication(new Variable("x"), new Constant(0)));
        assertEquals(true, multiplication.differentiate(x).equals(expected));
        
        Expression multiplication2 = new Multiplication(new Variable("y"), new Variable("x"));
        Expression expected2 = new Addition(
                new Multiplication(new Variable("y"), new Constant(1)),
                new Multiplication(new Variable("x"), new Constant(0)));
        assertEquals(true, multiplication2.differentiate(x).equals(expected2));
        assertEquals(true, expected2.equals(multiplication2.differentiate(x)));
        
        
    }
    
    @Test
    public void testPolynomialAndMixed() {
        
        Variable x = new Variable("x");
        
        Multiplication x2 = new Multiplication(x, x);
        Multiplication m1b = new Multiplication(x2, x2);
        Multiplication m2a = new Multiplication(new Constant(3), x2);
        assertEquals(m1b.differentiate(x).hashCode(), m2a.hashCode());
        
        Variable y = new Variable("y");
        Constant c1 = new Constant(12);
        Constant c2 = new Constant(103.32);
        Expression test = new Addition(
                new Multiplication(x,c2),
                new Addition(y, c1)
        );
        assertEquals(test.differentiate(x).hashCode(), c2.hashCode());
    }
    
}
