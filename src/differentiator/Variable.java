package differentiator;

public class Variable implements Expression {

    private final String name;
    private final String type;
    
    @Override
    public Expression getLeft(){
        return this;
    }
    
    @Override
    public Expression getRight(){
        return this;
    }
    
    @Override
    public String getType(){
        return this.type;
    }
    
    @Override
    public double getValue(){
        // Returns non number since variable doesn't have a set value
        return Double.NaN;
    }
    
    // Literal Term
    public Variable(String name){
        this.name = name;
        this.type = "Variable";
    }
    
    @Override
    public String toString(){
        return this.name;
    }
    
    @Override
    public boolean equals(Expression expression){
        return this.getClass() == expression.getClass() && this.name.equals(expression.toString());
    }
    
    @Override
    public int hashCode(){
        return this.name.hashCode();
    }
    
    @Override
    public Expression differentiate(Expression variable){
        if (variable.getClass() == this.getClass() && variable.toString().equals(this.name)){
            return new Constant(1);
        } else {
            return new Constant(0);
        }
    }
    
}
