package differentiator;

import java.util.Stack;

import differentiator.grammar.ExpressionBaseListener;
import differentiator.grammar.ExpressionLexer;
import differentiator.grammar.ExpressionParser;

/**
 * Write your listener here. You should override the methods in
 * ExpressionBaseListener.java.
 */
public class Listener extends ExpressionBaseListener {
    /*
     * Here are two sample functions one might have in a listener.
     *
    @Override
    public void enterLine(ExpressionParser.LineContext ctx) {
        System.out.println("Entered line rule!");
        System.out.println("Rule has token " + ctx.PLUS() + " with text " +
                           ctx.PLUS().getText());
    }
    @Override
    public void exitLine(ExpressionParser.LineContext ctx) {
        System.out.println("Exited line rule!");
        System.out.println("Rule has token " + ctx.PLUS() + " with text " +
                           ctx.PLUS().getText());
    }
     */
    
    // Utilizing Last in, first out for addition / multiplication grouping
    private Stack<Expression> stack = new Stack<Expression>();
    
    @Override
    public void exitLine(ExpressionParser.LineContext ctx) {
        assert stack.size() == 1;
    }
    
    @Override
    public void exitExpression(ExpressionParser.ExpressionContext ctx) {
        // Expression (Addition) Rule: multiplication + multiplication + ...
        // Groups all possible additions (expressions) within enclosed parentheses
        if (ctx.multiplication().size() > 1){
            // Group multiplications additions by two starting with one and absorbing consecutive ones
            for (int i = 1; i < ctx.multiplication().size(); i++){
                Expression right = stack.pop();
                Expression left = stack.pop();
                Expression add = new Addition(left, right);
                stack.push(add);
            }
        }        
    }
    
    @Override
    public void exitMultiplication(ExpressionParser.MultiplicationContext ctx) {
        // Multiplication Rule: atom * atom * atom * ...
        // Groups all possible products within enclosed parentheses
        if (ctx.atom().size() > 1) {
            // Group atoms factors by two starting with one and absorbing consecutive ones
            for (int i = 1; i < ctx.atom().size(); i++){
                Expression right = stack.pop();
                Expression left = stack.pop();
                Expression mult = new Multiplication(left, right);
                stack.push(mult);
            }
        }
    }
    
    @Override
    public void exitAtom(ExpressionParser.AtomContext ctx) {
        
        // Literal Number Creates a Constant
        if (ctx.start.getType() == ExpressionLexer.NUMBER){
            String numString = ctx.NUMBER().toString();
            if (Double.parseDouble(numString) % 1.0 != 0.0){
                double num = Double.parseDouble(numString);
                stack.push(new Constant(num));
            } else {
                int num = (int)Double.parseDouble(numString);
                stack.push(new Constant(num));
            }
        } 
        // Literal Var Creates a Variable
        else if (ctx.start.getType() == ExpressionLexer.VAR){
            String name = ctx.VAR().toString();
            stack.push(new Variable(name));
        } 
        // Grouping of Expression with Parentheses.  NO mathematical difference
        // Operations grouped into parentheses become a new "atom"
        else {
            return;
        }
    }
    
    public Expression getExpression() {
        return stack.get(0);
    }
}