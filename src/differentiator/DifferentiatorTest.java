package differentiator;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * This is the test suite for Differentiator. You should expand this file with
 * more unit tests to make sure your Differentiator works correctly.
 */
public class DifferentiatorTest {

    // Helper function to test if certain strings compile
    public void parseHelper(String input){
        new Differentiator().parse(input);
    }
    
    @Test
    public void testDifferentiatePublic() {
        new Differentiator().apply("x", "x");
        // this tests only that it compiles and doesn't throw an exception
    }

    @Test
    public void testParseCompiles() {
        new Differentiator().parse("x");
        // this tests only that it compiles and doesn't throw an exceptionx
    }
    
    // Test that Addition Parses OK
    @Test
    public void testAddition() {
        parseHelper(" 1 * 2.0 + x * 3 * 3 ");
        parseHelper("1+2.0+x");  
    }
    
    // Test that Multiplication Parses OK
    @Test 
    public void testMultiplication() {
        parseHelper(" (1 * 2.0) * x ");
        parseHelper("1*2.0*x");
    }
    
    // Test that Parentheses Parses OK
    @Test
    public void testParentheses() {
        parseHelper("(89 + (23))");
        parseHelper("((((((((((0))))))))))");
    }
    
    // Test that Mixed Operations, Spacing, Order of Operations Parse OK
    @Test
    public void testMixed() {
        parseHelper("3 * (x + 2.4 )");
        parseHelper("( 3+ 4 ) *x*x");
        parseHelper("foo + bar + baz");
        parseHelper("( 2*x    )+     (   y*x     )");
        parseHelper("(((foo + x)))");
        parseHelper("4+ 3*x+ 2*x*x+ 1*x*x*x");
        parseHelper("18+2* 3+x+y");
        parseHelper("y * x + 10");
    }
    
    // Test to make sure that Differentiator parses the correct Expression object
    @Test
    public void testParseEquals() {
        Differentiator differentiator = new Differentiator();
        Variable x = new Variable("x");
        Constant one = new Constant(1);
        Expression expected = new Addition(one, x);
        assertEquals(true, expected.hashCode() == differentiator.parse("x+1").hashCode());
    }
    
    
    // Helper Function to Help Test Differentiator.apply()
    public void applyHelper(String differential, String variable,  String expected) {
        assertEquals(true, new Differentiator().apply(differential, variable).equals(expected));
    }
    
    @Test
    public void testApply() {
        applyHelper("0", "x", "0");
        applyHelper("x*3", "x", "3");
        applyHelper("x+3", "x", "1");
        applyHelper("x+2", "y", "0");
        applyHelper("x*x*x", "x", "((x * (x + x)) + (x * x))");
    }

}