package differentiator;

public class Addition implements Expression{
    
    private final Expression left;
    private final Expression right;
    private final String type;
    
    @Override
    public Expression getLeft(){
        return this.left;
    }
    
    @Override
    public Expression getRight(){
        return this.right;
    }
    
    @Override
    public String getType(){
        return this.type;
    }
    
    @Override
    public double getValue(){
        // Continues to Return NaN values if branches contained variables. Otherwise 
        // return the sum of previous expressions
        if (this.left.getValue() == Double.NaN || this.right.getValue() == Double.NaN){
            return Double.NaN;
        } else {
            return this.left.getValue() + this.right.getValue();
        }
    }
    
    public Addition(Expression left, Expression right){
        this.left = left;
        this.right = right;
        this.type = "Addition";
    }
    
    @Override
    public String toString(){
        if (this.right.getValue() == 0){ return this.left.toString(); }
        else if (this.left.getValue() == 0){ return this.right.toString(); }
        else { return "(" + left.toString() + " + " + right.toString() + ")"; }
    }
    
    @Override
    public boolean equals(Expression expression){
        return this.getClass() == expression.getClass() && 
                this.getLeft().equals(expression.getLeft()) && 
                this.getRight().equals(expression.getRight());
    }
    
    @Override
    public int hashCode(){
        if (this.getValue() != Double.NaN){
            return new Double(this.getValue()).hashCode();
        } else {
            return this.left.hashCode() + this.right.hashCode();
        }
    }
    
    @Override
    public Expression differentiate(Expression variable){
        Expression output = new Addition(
                this.left.differentiate(variable),
                this.right.differentiate(variable)
        );
        return output;
    }

}
