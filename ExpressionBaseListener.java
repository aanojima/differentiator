// Generated from src/differentiator/grammar/Expression.g4 by ANTLR 4.0

package differentiator.grammar;


import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.antlr.v4.runtime.tree.ErrorNode;

public class ExpressionBaseListener implements ExpressionListener {
	@Override public void enterLine(ExpressionParser.LineContext ctx) { }
	@Override public void exitLine(ExpressionParser.LineContext ctx) { }

	@Override public void enterEveryRule(ParserRuleContext ctx) { }
	@Override public void exitEveryRule(ParserRuleContext ctx) { }
	@Override public void visitTerminal(TerminalNode node) { }
	@Override public void visitErrorNode(ErrorNode node) { }
}