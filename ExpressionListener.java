// Generated from src/differentiator/grammar/Expression.g4 by ANTLR 4.0

package differentiator.grammar;

import org.antlr.v4.runtime.tree.*;
import org.antlr.v4.runtime.Token;

public interface ExpressionListener extends ParseTreeListener {
	void enterLine(ExpressionParser.LineContext ctx);
	void exitLine(ExpressionParser.LineContext ctx);
}